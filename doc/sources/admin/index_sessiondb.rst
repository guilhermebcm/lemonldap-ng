Sessions database
=================

.. toctree::
   :maxdepth: 1

   changesessionbackend
   filesessionbackend
   sqlsessionbackend
   ldapsessionbackend
   nosqlsessionbackend
   mongodbsessionbackend
   browseablesessionbackend
   restsessionbackend
   soapsessionbackend
